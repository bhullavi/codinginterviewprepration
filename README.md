# DataStructures and Algorithms

This repo is intended to be used for practical prep to common coding problems. Please go through each problem and attempt to solve yourself withoout using help. 

Ensure that you have the below, before attempting to run the tests
1. Code editor - Visual Studio Code or Editor like Atom, Sublime 
2. Ensure that you have node.js installed in your machine
3. Verify that node.js is infact installed by executing the below node -v

Steps to get up and running

1. Clone the repo
2. Navigate to the cloned repo by using - cd (name of cloned folder)
3. Open terminal
4. Install JEST
	- sudo npm install -g jest
5. Go the the exercies folder and excute the below 
	- jest {problem}/test.js --watch
	
As you make changes to the solution, the unit tests will run automatically and tell you, if you're writing a valid solution or not.

This project is integrated with Jenkins instance and will automatically execute the unit tests. 